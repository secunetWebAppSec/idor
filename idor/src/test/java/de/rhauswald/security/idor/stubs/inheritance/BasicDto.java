/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.stubs.inheritance;

import de.rhauswald.security.idor.IdorProtectedField;

public abstract class BasicDto {
	@IdorProtectedField
	private String id;
	private String nonProtectedField;

	//Fields which may cause trouble to the reflection scanner
	@SuppressWarnings("UnusedDeclaration")
	private String fieldWithoutGetterAndSetter;
	@SuppressWarnings({"UnusedDeclaration", "FieldCanBeLocal"})
	private String fieldWithoutGetter;
	@SuppressWarnings("UnusedDeclaration")
	private String fieldWithoutSetter;

	private String fieldWithPrivateSetterAndGetter;
	private String fieldWithDefaultSetterAndGetter;
	private String fieldWithProtectedSetterAndGetter;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNonProtectedField() {
		return nonProtectedField;
	}

	public void setNonProtectedField(String nonProtectedField) {
		this.nonProtectedField = nonProtectedField;
	}

	public String getFieldWithoutSetter() {
		return fieldWithoutSetter;
	}

	public void setFieldWithoutGetter(String fieldWithoutGetter) {
		this.fieldWithoutGetter = fieldWithoutGetter;
	}

	private String getFieldWithPrivateSetterAndGetter() {
		return fieldWithPrivateSetterAndGetter;
	}

	private void setFieldWithPrivateSetterAndGetter(String fieldWithPrivateSetterAndGetter) {
		this.fieldWithPrivateSetterAndGetter = fieldWithPrivateSetterAndGetter;
	}

	String getFieldWithDefaultSetterAndGetter() {
		return fieldWithDefaultSetterAndGetter;
	}

	void setFieldWithDefaultSetterAndGetter(String fieldWithDefaultSetterAndGetter) {
		this.fieldWithDefaultSetterAndGetter = fieldWithDefaultSetterAndGetter;
	}

	protected String getFieldWithProtectedSetterAndGetter() {
		return fieldWithProtectedSetterAndGetter;
	}

	protected void setFieldWithProtectedSetterAndGetter(String fieldWithProtectedSetterAndGetter) {
		this.fieldWithProtectedSetterAndGetter = fieldWithProtectedSetterAndGetter;
	}
}
