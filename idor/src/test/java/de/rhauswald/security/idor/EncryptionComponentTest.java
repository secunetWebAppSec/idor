/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor;

import de.rhauswald.security.idor.internal.encryption.EncryptionComponentBasic;
import org.apache.shiro.crypto.DefaultBlockCipherService;
import org.apache.shiro.crypto.OperationMode;
import org.apache.shiro.crypto.PaddingScheme;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import java.security.Key;
import java.security.Security;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class EncryptionComponentTest {
	private EncryptionComponentBasic encryptionComponent;

	@Before
	public void setUp() throws Exception {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
		final DefaultBlockCipherService cipherService = new DefaultBlockCipherService("AES");
        cipherService.setMode(OperationMode.GCM);
        cipherService.setPaddingScheme(PaddingScheme.NONE);
		final Key key = cipherService.generateNewKey(128);
		this.encryptionComponent = new EncryptionComponentBasic(new SecretKeyProvider() {
			@Override
			public byte[] provide() {
				return key.getEncoded();
			}
		});
	}

	@Test
	public void testEncryptDecrypt() throws Exception {
		final String cipherText = encryptionComponent.encryptToBase64("Hallo Welt");
		final String plainText = encryptionComponent.decryptFromBase64(cipherText);
		assertThat(plainText, is(equalTo("Hallo Welt")));
	}
}
