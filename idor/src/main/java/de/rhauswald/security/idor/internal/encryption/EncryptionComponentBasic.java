/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.encryption;

import de.rhauswald.security.idor.EncryptionComponent;
import de.rhauswald.security.idor.SecretKeyProvider;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.CipherService;
import org.apache.shiro.crypto.DefaultBlockCipherService;
import org.apache.shiro.crypto.OperationMode;
import org.apache.shiro.crypto.PaddingScheme;
import org.apache.shiro.util.ByteSource;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

public class EncryptionComponentBasic implements EncryptionComponent {
    private final SecretKeyProvider secretKeyProvider;
    private final CipherService cipherService;

    public EncryptionComponentBasic(SecretKeyProvider secretKeyProvider) {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        this.secretKeyProvider = secretKeyProvider;
        final DefaultBlockCipherService defaultBlockCipherService = new DefaultBlockCipherService("AES");
        defaultBlockCipherService.setMode(OperationMode.GCM);
        defaultBlockCipherService.setPaddingScheme(PaddingScheme.NONE);
        cipherService = defaultBlockCipherService;
    }

    @Override
    public String encryptToBase64(String plainText) {
        final byte[] encryptionKey = secretKeyProvider.provide();
        final ByteSource encrypt = cipherService.encrypt(plainText.getBytes(), encryptionKey);
        final String base64 = encrypt.toBase64();
        return base64;
    }

    @Override
    public String decryptFromBase64(String encrypted) {
        final byte[] decryptionKey = secretKeyProvider.provide();
        final byte[] decode = Base64.decode(encrypted);
        final ByteSource decrypt = cipherService.decrypt(decode, decryptionKey);
        return new String(decrypt.getBytes());
    }
}
