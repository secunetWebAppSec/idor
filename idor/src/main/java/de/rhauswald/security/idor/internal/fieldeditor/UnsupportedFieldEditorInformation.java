/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.fieldeditor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

class UnsupportedFieldEditorInformation implements FieldEditorInformation {
	private final Field field;

	public UnsupportedFieldEditorInformation(Field field) {
		this.field = field;
	}

	public Field getField() {
		return field;
	}

	public Method getGetter() {
		throw new UnsupportedOperationException();
	}

	public Method getSetter() {
		throw new UnsupportedOperationException();
	}

	public boolean isSupported() {
		return false;
	}

	@Override
	@SuppressWarnings("RedundantIfStatement")
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UnsupportedFieldEditorInformation that = (UnsupportedFieldEditorInformation) o;

		if (field != null ? !field.equals(that.field) : that.field != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return field != null ? field.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "UnsupportedFieldEditorInformation{" +
				"field=" + field +
				'}';
	}
}
