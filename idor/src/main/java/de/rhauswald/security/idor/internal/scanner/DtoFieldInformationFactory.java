/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner;

import com.gs.collections.api.block.function.Function2;
import de.rhauswald.security.idor.internal.DtoFieldInformation;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditor;

//TODO: remove function 2 stuff
class DtoFieldInformationFactory implements Function2<FieldEditorInformation, Object, DtoFieldInformation> {
    private DtoFieldInformationFactory() {
    }

    @Override
	public DtoFieldInformation value(FieldEditorInformation fieldEditorInformation, Object declaringObject) {
		final Object value = FieldEditor.getValue(fieldEditorInformation, declaringObject);
		return new DtoFieldInformation(declaringObject, fieldEditorInformation, value);
	}

    public static DtoFieldInformationFactory mapToDtoFieldInformation() {
        return new DtoFieldInformationFactory();
    }
}
