/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.annotations;


import com.gs.collections.api.block.predicate.Predicate;
import com.gs.collections.api.list.ImmutableList;
import de.rhauswald.security.idor.internal.DtoFieldInformation;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import static de.rhauswald.security.idor.internal.annotations.AnnotationByClassPredicate.acceptOnlyAnnotation;
import static de.rhauswald.security.idor.internal.annotations.FieldToDeclaredAnnotationsMapper.declaredAnnotations;

public class DtoFieldInformationWithDeclaredAnnotationPredicate implements Predicate<DtoFieldInformation> {
    private final Class<? extends Annotation> annotationClass;

    private DtoFieldInformationWithDeclaredAnnotationPredicate(Class<? extends Annotation> annotationClass) {
        this.annotationClass = annotationClass;
    }

    @Override
    public boolean accept(DtoFieldInformation each) {
        final FieldEditorInformation fieldEditorInformation = each.getFieldEditorInformation();
        final Field field = fieldEditorInformation.getField();
        final ImmutableList<Annotation> declaredAnnotations = declaredAnnotations(field);
        final Annotation annotation = declaredAnnotations.detect(acceptOnlyAnnotation(annotationClass));
        final boolean accept = annotation != null;
        return accept;
    }

    public static DtoFieldInformationWithDeclaredAnnotationPredicate acceptOnlyFieldsWithAnnotation(Class<? extends Annotation> annotationClass) {
        return new DtoFieldInformationWithDeclaredAnnotationPredicate(annotationClass);
    }
}
