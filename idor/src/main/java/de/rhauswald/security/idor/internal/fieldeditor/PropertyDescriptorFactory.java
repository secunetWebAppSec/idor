/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.fieldeditor;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

class PropertyDescriptorFactory {
    private PropertyDescriptorFactory() {
    }

    public static PropertyDescriptor fromField(Field field) {
        try {
            final PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), field.getDeclaringClass());
            final Method readMethod = propertyDescriptor.getReadMethod();
            failForStaticMethod(readMethod);
            final Method writeMethod = propertyDescriptor.getWriteMethod();
            failForStaticMethod(writeMethod);
            return propertyDescriptor;
        } catch (IntrospectionException e) {
            throw new UnsupportedFieldException("Could not create PropertyDescriptor from field " + field, e);
        }
    }

    private static void failForStaticMethod(Method method) {
        if (Modifier.isStatic(method.getModifiers())) {
            throw new UnsupportedFieldException("getters and setters must not be static but " + method + " is static.");
        }
    }

    public static class UnsupportedFieldException extends RuntimeException {
        public UnsupportedFieldException(String message) {
            super(message);
        }

        public UnsupportedFieldException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
