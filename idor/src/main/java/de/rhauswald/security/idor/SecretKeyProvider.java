/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor;

import org.apache.shiro.crypto.DefaultBlockCipherService;
import org.apache.shiro.crypto.OperationMode;
import org.apache.shiro.crypto.PaddingScheme;

import java.security.Key;

public interface SecretKeyProvider {
	byte[] provide();
    public static class Factory {
        public static SecretKeyProvider basicSecretKeyProvider() {
            return new SecretKeyProvider() {
                public byte[] keyEncoded;

                @Override
                public synchronized byte[] provide() {
                    if (keyEncoded == null) {
                        final DefaultBlockCipherService defaultBlockCipherService = new DefaultBlockCipherService("AES");
                        defaultBlockCipherService.setMode(OperationMode.GCM);
                        defaultBlockCipherService.setPaddingScheme(PaddingScheme.NONE);
                        final Key key = defaultBlockCipherService.generateNewKey(128);
                        keyEncoded = key.getEncoded();
                    }
                    return keyEncoded;
                }
            };
        }
    }
}
